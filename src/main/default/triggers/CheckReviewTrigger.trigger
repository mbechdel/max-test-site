/**
 * Trigger to create a case when a review is created that has a review
 * value below a certain threshold. Also, puts the case in a specific
 * queue.
 */
trigger CheckReviewTrigger on Book_Review__c (after insert) {
    List<Case> newCases = new List<Case>();

    // get the queue the case should be assigned to
    Group queue = [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND Name = 'Low Book Reviews'];

    Set<Id> bookIds = new Set<Id>();
    for (Book_Review__c review : Trigger.new) {
        bookIds.add(review.Book_FK__c);
    }

    Map<Id, Book__c> books = new Map<Id, Book__c>([SELECT Id, Name FROM Book__c WHERE Id IN :bookIds]);

    for (Book_Review__c review : Trigger.new) {
        // check if the review is below the threshold
        if (review.Review_Value__c <= 1) {
            // get the book name so we can use it for the case subject
            //Book__c book = [SELECT Name FROM Book__c WHERE Id = :review.Book_FK__c];
            Book__c book = books.get(review.Book_FK__c);

            // create case
            newCases.add(new Case(
                Book_Review__c = review.Id,
                Subject = 'Low Book Review - Book: ' + book.Name,
                OwnerId = queue.Id
            ));
        }
    }

    insert newCases;
}