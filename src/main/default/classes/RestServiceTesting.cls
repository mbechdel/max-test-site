@RestResource(urlMapping='/myservice')
global with sharing class RestServiceTesting {
    @HttpPost
     global static Account doPost(Account acc) {
        return acc;
    }
}