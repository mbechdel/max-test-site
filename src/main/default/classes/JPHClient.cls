/**
 * JSON Placeholder (JPH) Client.
 * Built by Maximus for Skill Challenge: https://callawaycloud.my.salesforce.com/a0K1500000EnKC7.
 */
public class JPHClient {

    private static final String ENDPOINT = 'http://jsonplaceholder.typicode.com/';

    // Sends a request to JPH.
    public static HTTPResponse request(String method, String resource) {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(ENDPOINT + resource);
        request.setMethod(method);

        Http http = new Http();
        HTTPResponse response = http.send(request);

        handleResponse(response);
        return response;
    }

    // Handles HTTP Respone and throws exception if the response status code is not in the 200s.
    private static void handleResponse(HttpResponse response) {
        if (response.getStatusCode() >= 300 || response.getStatusCode() < 200) {
            throw new JPH_Exception('Status code ' + response.getStatusCode() + '. Body: ' + response.getBody());
        }
    }
    public class JPH_Exception extends Exception {}
}