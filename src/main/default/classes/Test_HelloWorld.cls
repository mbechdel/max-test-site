/**
 * Unit tests for the HelloWorld class.
 */

@isTest
private class Test_HelloWorld {
	
    /**
     * A most basic assert.
     */
	@isTest static void testOutput() {
		HelloWorld hw = new HelloWorld();
		System.assertEquals('Hello World', hw.output);
	}

    @isTest static void testMap() {
    	HelloWorld hw = new HelloWorld();

    	// test initalization, add a value, test the new value
    	System.debug('Init: ' + hw.hwMap);
    	System.assertEquals('', hw.hwMap.get(HelloWorld.HELLO_WORLD_GETTER));
    	hw.setValue('Hello World', 'Kylo Ren');
    	System.debug('After adding value: ' + hw.hwMap);
    	System.assertEquals('Kylo Ren', hw.hwMap.get('Hello World'));
    }

    @isTest static void testLabel() {
    	System.assertEquals(HelloWorld.COMPANY_NAME_ALTERNATE, Label.company_name_alternate);
    }    
}