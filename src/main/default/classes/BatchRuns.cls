public class BatchRuns implements Database.Batchable<sObject> {
	
    /*
     * add field, Book_Abbreviation__c, on Book__c object       DONE
     * create batch script to set Book_Abbreviation__c          DONE
     * create unit test that makes 10,000 books                 DONE
     * then in unit test run the batch script                   DONE
     * then in unit test verify Book_Abbreviation__c was set    NOT DONE
     */

	String query;

	public BatchRuns() {
	}
	
	public Database.QueryLocator start(Database.BatchableContext bC) {

        query = 'SELECT Id, Name, Book_Abbreviation__c FROM Book__c';

		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext bC, List<Book__c> books) {
        // set the abbreviated book name
        for (Book__c book : books) {
            book.Book_Abbreviation__c = book.name.abbreviate(15);
        }

        // now update the database
        update books;
	}
	
	public void finish(Database.BatchableContext bC) {
	}
}