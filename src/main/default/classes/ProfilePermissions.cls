public with sharing class ProfilePermissions {
    public ProfilePermissions() {
        createSSO();

        Super_Secret_Object__c sso = returnSSO();
        System.debug('Constructor sso.Name = ' + sso.Name);
    }

    public static void createSSO() {
        Super_Secret_Object__c sso = new Super_Secret_Object__c(Name='22');

        insert sso;
    }

    public Super_Secret_Object__c returnSSO() {
        Super_Secret_Object__c[] sso = [SELECT Name FROM Super_Secret_Object__c WHERE Name='22'];

        return sso[0];
    }
}