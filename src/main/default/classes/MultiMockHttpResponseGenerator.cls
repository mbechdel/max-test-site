public class MultiMockHttpResponseGenerator implements HttpCalloutMock {
    Map<String, HttpCalloutMock> requests;

    public MultiMockHttpResponseGenerator(Map<String, HttpCalloutMock> requests) {
        this.requests = requests;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpCalloutMock mock = requests.get(req.getEndpoint());
        if (mock != null) {
            return mock.respond(req);
        } else {
                throw new MultiMock_Exception('HTTP callout not supported for test methods');
        }
    }
    public class MultiMock_Exception extends Exception {}

    public void addRequestMock(String url, HttpCalloutMock mock) {
        requests.put(url, mock);
    }
}