/**
 * JSON Placeholder Posts client. Updates users before posts.
 * Built by Maximus for Skill Challenge: https://callawaycloud.my.salesforce.com/a0K1500000EnKC7.
 */
public class JPHPosts {
    public static void updateUserPosts() {
        // make our REST calls
        HttpResponse usersResponse = JPHClient.request('GET', 'users');
        HttpResponse postsResponse = JPHClient.request('GET', 'posts');

System.debug('Code===> ' + usersResponse.getStatusCode());
System.debug('Status===> ' + usersResponse.getStatus());
for (String headerKey : usersResponse.getHeaderKeys()) {
    System.debug('Header===> ' + headerKey);// + ' : ' + usersResponse.getHeader(headerKey));
}
System.debug('Body===> ' + usersResponse.getBody());

        // convert the responses to objects and upsert them to SF
        List<User> users = (List<User>) JSON.deserialize(usersResponse.getBody(), List<User>.class);
        List<Post> posts = (List<Post>) JSON.deserialize(postsResponse.getBody(), List<Post>.class);

        updateContacts(users);
        updatePosts(posts, users);
    }


    // Create SF Contact records from the Users object list and upsert them into Salesforce.
    private static void updateContacts(List<User> users) {
        List<Contact>       contactsToUpsert    = new List<Contact>();
        Set<String>         websites            = new Set<String>(); // unique list of websites used later to query accounts
        Map<String, String> contactToWebsite    = new Map<String, String>();

        // build out our list of contacts to upsert
        for (User user : users) {
            // split up full name to first and last name
            String firstName = user.name.Substring(0, user.name.indexOf(' '));
            String lastName = user.name.Substring(user.name.indexOf(' '), user.name.length());

            websites.add(user.website);

            // keep track of each contacts "website" (used to map to their account)
            contactToWebsite.put(String.valueOf(user.id), user.website);

            contactsToUpsert.add( new Contact(
                Author_Id__c = String.valueOf(user.id),
                FirstName = firstName,
                LastName = lastName,
                Email = user.email,
                Phone = user.phone,
                Title = 'JPH User',
                MailingStreet = user.address.street + '\n' + user.address.suite,
                MailingCity = user.address.city,
                MailingPostalCode = user.address.zipcode,
                MailingLatitude = Double.valueOf(user.address.geo.lat),
                MailingLongitude = Double.valueOf(user.address.geo.lng)
            ));
        }

        // get the accounts we need
        List<Account> accounts = [SELECT Id, Website FROM Account WHERE Website IN :websites];

        // update the contact records account id now that we have a list of of accounts
        for (Contact contact : contactsToUpsert) {
            contact.AccountId = getAcctIdByWebsite(accounts, contactToWebsite.get(contact.Author_Id__c));
        }
        upsert contactsToUpsert Author_Id__c;
    }

    // Find the Id of an Account based on the Website field.
    private static Id getAcctIdByWebsite(List<Account> accounts, String website) {
        for (Account account : accounts) {
            if (account.Website == website) {
                return account.Id;
            }
        }
        throw new JPHClient.JPH_Exception('COULD NOT FIND ACCOUNT WITH WEBSITE: ' + website);
    }


    // Create SF Post records from the Post object list and upsert them into Salesforce.
    private static void updatePosts(List<Post> posts, List<User> users) {
        List<Post__c> postsToUpsert = new List<Post__c>();

        // build map of Contact.Id->Contact.Author_Id__c
        Set<String> authIds = new Set<String>();
        for (User user : users) {
            authIds.add(String.valueOf(user.id));
        }
        List<Contact> contacts = [SELECT Id, Author_Id__c FROM Contact WHERE Author_Id__c IN :authIds];

        for (Post post : posts) {
            postsToUpsert.add(new Post__c(
                Author__c   = getContactIdByAuthId(contacts, String.valueOf(post.userId)),
                Name        = post.title.abbreviate(80),
                Post_Id__c  = String.valueOf(post.id),
                Title__c    = post.title,
                Body__c     = post.body
            ));
        }
        upsert postsToUpsert Post_Id__c;
    }

    // Find the Id of a Contact based on the Author_Id__c.
    private static Id getContactIdByAuthId(List<Contact> contacts, String userId) {
        for (Contact contact : contacts) {
            if (contact.Author_Id__c == userId) {
                return contact.Id;
            }
        }
        throw new JPHClient.JPH_Exception('COULD NOT FIND CONTACT WITH Author_Id__c: ' + userId);
    }


    public class User {
        Integer id          { get; set; }
        String  name        { get; set; }
        String  username    { get; set; }
        String  email       { get; set; }
        Address address     { get; set; }
        String  phone       { get; set; }
        String  website     { get; set; }
        Company company     { get; set; }
    }
    public class Company {
        String name         { get; set; }
        String catchPhrase  { get; set; }
        String bs           { get; set; }
    }
    public class Address {
        String  street  { get; set; }
        String  suite   { get; set; }
        String  city    { get; set; }
        String  zipcode { get; set; }
        Geo     geo     { get; set; }
    }
    public class Geo {
        String lat { get; set; }
        String lng { get; set; }
    }
    public class Post {
        Integer userId  { get; set; }
        Integer id      { get; set; }
        String  title   { get; set; }
        String  body    { get; set; }
    }
}