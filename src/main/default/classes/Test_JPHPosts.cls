@isTest
private class Test_JPHPosts {



    // tests that the endpoint works (mock callout)
    @isTest static void testUsersResponse() {
        // set mock callout
        MockHttpResponseGenerator usersMock = setupUsersMock();
        Test.setMock(HttpCalloutMock.class, usersMock);
        HttpResponse usersResponse = JPHClient.request('GET', 'users');

        System.assertEquals(usersMock.code, usersResponse.getStatusCode());
        System.assertEquals(usersMock.status, usersResponse.getStatus());
        System.assertEquals(usersMock.body, usersResponse.getBody());
    }

    // tests that the endpoint works (mock callout)
    @isTest static void testPostsResponse() {
        // set mock callout
        MockHttpResponseGenerator postsMock = setupPostsMock();
        Test.setMock(HttpCalloutMock.class, postsMock);
        HttpResponse usersResponse = JPHClient.request('GET', 'posts');

        System.assertEquals(postsMock.code, usersResponse.getStatusCode());
        System.assertEquals(postsMock.status, usersResponse.getStatus());
        System.assertEquals(postsMock.body, usersResponse.getBody());
    }

    @isTest static void testMulitMock() {
        setup();
        MockHttpResponseGenerator usersMock = setupUsersMock();
        MockHttpResponseGenerator postsMock = setupPostsMock();

        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('http://jsonplaceholder.typicode.com/users',usersMock);
        endpoint2TestResp.put('http://jsonplaceholder.typicode.com/posts',postsMock);

        HttpCalloutMock multiCalloutMock = new MultiMockHttpResponseGenerator(endpoint2TestResp);

        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

        Test.startTest();
        JPHPosts.updateUserPosts();
        Test.stopTest();

        List<Contact> contacts = [SELECT Id FROM Contact];
        System.assertEquals(2, contacts.size());

        Contact contact = [SELECT Author_Id__c, FirstName, LastName, MailingStreet FROM Contact WHERE Author_Id__c = '1'];
        System.assert(contact != null);
        System.assertEquals('1', contact.Author_Id__c);
        System.assertEquals('max', contact.FirstName);
        System.assertEquals('bechdel', contact.LastName);
        System.assertEquals('Kulas Light\nApt. 556', contact.MailingStreet);

        List<Post__c> posts = [SELECT Id FROM Post__c];
        System.assertEquals(4, posts.size());

        Post__c post = [SELECT Title__c, Body__c FROM Post__c WHERE Post_Id__c = '1'];
        System.assert(post != null);
        System.assertEquals('sunt au1', post.Title__c);
        System.assertEquals('quia et suscipit suscipit recusanda1', post.Body__c);
    }

    private static MockHttpResponseGenerator setupUsersMock() {
        Integer code = 200;
        String status = 'OK';
        Map<String, String> responseHeaders = new Map<String, String>(); // don't care

        String user1 =  '{"id": 1,"name": "max bechdel","username": "mbechdel","email": "mbechdel@gmail.com",';
        user1 += '"address": {"street": "Kulas Light","suite": "Apt. 556","city": "Gwenborough","zipcode": "92998-3874",';
        user1 += '"geo": {"lat": "-37.3159","lng": "81.1496"}},';
        user1 += '"phone": "1-770-736-8031 x56442","website": "hildegard.org",';
        user1 += '"company": {"name": "Romaguera-Crona","catchPhrase": "Multi-layered client-server neural-net","bs": "harness real-time e-markets"}}';

        String user2 =  '{"id": 2,"name": "max bechdel","username": "mbechdel","email": "mbechdel@gmail.com",';
        user2 += '"address": {"street": "Kulas Light","suite": "Apt. 556","city": "Gwenborough","zipcode": "92998-3874",';
        user2 += '"geo": {"lat": "-37.3159","lng": "81.1496"}},';
        user2 += '"phone": "1-770-736-8031 x56442","website": "blah.org",';
        user2 += '"company": {"name": "Romaguera-Crona","catchPhrase": "Multi-layered client-server neural-net","bs": "harness real-time e-markets"}}';

        String body = '[' + user1 + ',' + user2 + ']';

        return new MockHttpResponseGenerator(code, status, body);
    }

    private static MockHttpResponseGenerator setupPostsMock() {
        Integer code = 200;
        String status = 'OK';
        Map<String, String> responseHeaders = new Map<String, String>(); // don't care
        String body =  '[{"userId": 1,"id": 1,"title": "sunt au1","body": "quia et suscipit suscipit recusanda1"},';
        body +=         '{"userId": 1,"id": 2,"title": "sunt au2","body": "quia et suscipit suscipit recusanda2"},';
        body +=         '{"userId": 2,"id": 3,"title": "sunt au3","body": "quia et suscipit suscipit recusanda3"},';
        body +=         '{"userId": 2,"id": 4,"title": "sunt au4","body": "quia et suscipit suscipit recusanda4"}]';

        return new MockHttpResponseGenerator(code, status, body);
    }

    private static void setup() {
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(
            Name = 'hildegard.org',
            Website = 'hildegard.org'
        ));
        accounts.add(new Account(
            Name = 'blah.org',
            Website = 'blah.org'
        ));
        insert accounts;
    }
}