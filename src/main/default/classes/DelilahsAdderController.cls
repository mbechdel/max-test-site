public class DelilahsAdderController {
    public static final Integer DEFAULT_NUM_SPRINKLES = 24;
    public String numSprinkles { get; set; }
    public Integer totalSprinkles { get; set; }
    public Boolean added { get; set; }

    public DelilahsAdderController() {
        added = false;
    }

    public PageReference add() {
        totalSprinkles = Integer.valueOf(numSprinkles) + getAddTo();
        added = true;
        return ApexPages.currentpage();
    }

    public Integer getAddTo() {
        if (String.isNotEmpty(ApexPages.currentpage().getparameters().get('addTo'))) {
            return Integer.valueOf(ApexPages.currentpage().getparameters().get('addTo'));
        } else {
            return DEFAULT_NUM_SPRINKLES;
        }
    }
}