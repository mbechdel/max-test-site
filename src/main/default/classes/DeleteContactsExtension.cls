/**
 *
 */
public with sharing class DeleteContactsExtension {

    public List<ContactRecord> contacts { get; set; }
    private ApexPages.StandardController controller;

    public DeleteContactsExtension(ApexPages.StandardController stdController) {
        controller = stdController;
        contacts = new List<ContactRecord>();

        for (Contact c : [SELECT Id, Name FROM Contact WHERE account.id = :controller.getId()]) {
            contacts.add(new ContactRecord(c));
        }
    }



    public class ContactRecord {
        public Contact contactObject { get; set; }
        public Boolean selected { get; set; }

        public ContactRecord(Contact c) {
            contactObject = c;
            selected = false;
        }
    }
}