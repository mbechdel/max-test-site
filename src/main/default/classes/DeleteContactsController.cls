public with sharing class DeleteContactsController {

    public List<ContactRecord> contacts { get; set; }
    private ApexPages.StandardController controller;

    public DeleteContactsController(ApexPages.StandardController stdController) {
        controller = stdController;
        contacts = new List<ContactRecord>();

        for (Contact c : [SELECT Id, Name FROM Contact WHERE account.id = :controller.getId()]) {
            contacts.add(new ContactRecord(c));
        }
    }

    /**
     * Delete selected records.
     */
    public PageReference deleteContacts() {
        List<Contact> contactsToDelete = new List<Contact>();

        // build list of contacts to delete
        for (ContactRecord contact : contacts) {
            if (contact.checked) {
                contactsToDelete.add(contact.contact);
            }
        }

        delete contactsToDelete;
        return controller.cancel(); // return back to account page
    }

    /**
     * Contact record containing the contain object and whether it has been selected for deletion.
     */
    public class ContactRecord {
        public Contact contact { get; set; }
        public Boolean checked { get; set; }

        public ContactRecord(Contact c) {
            contact = c;
            checked = false;
        }
    }
}