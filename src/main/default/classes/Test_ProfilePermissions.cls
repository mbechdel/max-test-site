@isTest
private class Test_ProfilePermissions {

    /*
     * Loads the default data, an account under "David's Profile".
     */
    @isTest static void testProfilePermissions() {
        ObjectPermissions[] sso = [SELECT SObjectType, PermissionsRead
                                        FROM ObjectPermissions
                                        WHERE parentid in (select id from permissionset where
                                        PermissionSet.Profile.Name = 'David\'s Profile')];

        Boolean foundObject = false;
        Boolean readPermissions = false;
        String objectName = 'Super_Secret_Object__c';

        // now loop through and make sure David's Profile doesn't have access to
        for (ObjectPermissions objectPermission : sso) {
            if(objectPermission.SObjectType == objectName) {
                foundObject = true;

                if(objectPermission.PermissionsRead == true) {
                    readPermissions = true;
                }

                // we're done, break out of loop
                break;
            }
        }

        System.assertEquals(false, foundObject, 'Object was found, and should not have been found.');
        System.assertEquals(false, readPermissions, 'Object found, and was also readable, and should not have been.');
    }

    @isTest static void testConstructor() {
        Profile p = [SELECT Id FROM Profile WHERE Name='David\'s Profile'];
        User u2 = new User(Alias = 'newUs22', Email='newus22@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='newus22@testorg.com');

        insert u2;

        System.runAs(u2) {
            ProfilePermissions pp = new ProfilePermissions();

            Super_Secret_Object__c[] ssos = [SELECT Name FROM Super_Secret_Object__c WHERE Name='22'];

            //if ( ssos.size() > 0 ) {
            //    System.debug( 'Super_Secret_Object__c.Name = ' + ssos[0].Name );
            //}

            System.assertEquals(1, ssos.size(), 'Could not find Super_Secret_Object__c with Name = 22');
        }
    }

    @isTest static void testRunAs() {
        Profile p = [SELECT Id FROM Profile WHERE Name='David\'s Profile'];
        User u2 = new User(Alias = 'newUs22', Email='newus22@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='newus22@testorg.com');

        insert u2;

        System.runAs(u2) {
            if (!Schema.SObjectType.Super_Secret_Object__c.fields.Name.isAccessible()) {
                System.debug('SSO is not accessible');
            } else {
                System.debug('SSSO is accessible');
            }
        }
    }

}