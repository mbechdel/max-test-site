@isTest
public class MockHttpResponseGenerator implements HttpCalloutMock {

    public Integer code;
    public String status;
    public String body;

    public MockHttpResponseGenerator(Integer code, String status, String body) {
        this.code = code;
        this.status = status;
        this.body = body;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }
}