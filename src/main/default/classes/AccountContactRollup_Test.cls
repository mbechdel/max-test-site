/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Test methods for first version of contact rollup
*/
@isTest
private class AccountContactRollup_Test {
    /* Test Methods */

    // Test that rollup works correctly for single record insert.
    @isTest static void insertTest() {
        // create test account and generate a related contact
        Account testAccount = createAccount();
        Contact testContact = generateContact(testAccount.Id);

        // force initial values to avoid any prior bugs
        forceRollup(testAccount.Id, 0);

        // insert contact
        Test.startTest();
        insert testContact;
        Test.stopTest();

        // validate rollup correct
        assertRollup(testAccount.Id, 1);
    }

    // Test that rollup works correctly for setting contact account
    @isTest static void testUpdateSetAccount() {
        // create test account and contact with no account
        Account testAccount = createAccount();
        Contact testContact = createContact(null);

        // force initial values to avoid any prior bugs
        forceRollup(testAccount.Id, 0);

        // set account id to test account
        testContact.AccountId = testAccount.Id;
        Test.startTest();
        update testContact;
        Test.stopTest();

        // validate rollup correct
        assertRollup(testAccount.Id, 1);
    }

    // Test that rollup works correctly for clearing contact account
    @isTest static void testUpdateClearAccount() {
        // create test account and two related contacts
        Account testAccount = createAccount();
        Contact testContact1 = generateContact(testAccount.Id);
        Contact testContact2 = generateContact(testAccount.Id);
        insert new Contact[] { testContact1, testContact2 };

        // force initial values to avoid any prior bugs
        forceRollup(testAccount.Id, 2);

        // clear account id and commit
        testContact1.AccountId = null;
        Test.startTest();
        update testContact1;
        Test.stopTest();

        // validate rollup correct
        assertRollup(testAccount.Id, 1);
    }

    // Test that rollup works correctly for switching from one account to another
    @isTest static void testUpdateChangeAccount() {
        // create two accounts, one with two contacts, and second with just one
        Account testAccount1 = generateAccount();
        Account testAccount2 = generateAccount();
        insert new Account[] { testAccount1, testAccount2 };
        Contact testContact1 = generateContact(testAccount1.Id);
        Contact testContact2 = generateContact(testAccount1.Id);
        Contact testContact3 = generateContact(testAccount2.Id);
        insert new Contact[] { testContact1, testContact2, testContact3 };

        // force intial rollup values
        forceRollup(new Map<Id, Integer>{ testAccount1.Id => 2, testAccount2.Id => 1 });

        // transfer contact one over to account 2
        testContact1.AccountId = testAccount2.Id;
        Test.startTest();
        update testContact1;
        Test.stopTest();

        // validate rollups correct
        assertRollup(new Map<Id, Integer>{ testAccount1.Id => 1, testAccount2.Id => 2});
    }

    // Test that rollup works correct when deleting contacts
    @isTest static void testDelete() {
        // create account with two contacts
        Account testAccount = createAccount();
        Contact testContact1 = generateContact(testAccount.Id);
        Contact testContact2 = generateContact(testAccount.Id);
        insert new Contact[] { testContact1, testContact2 };

        // force initial rollup values
        forceRollup(testAccount.Id, 2);

        // delete one contact
        Test.startTest();
        delete testContact1;
        Test.stopTest();

        // validate rollups correct
        assertRollup(testAccount.Id, 1);
    }

    // Test that rollup works correctly when undeleting contacts
    @isTest static void testUndelete() {
        // create account with two contacts, then delete one contact
        Account testAccount = createAccount();
        Contact testContact1 = generateContact(testAccount.Id);
        Contact testContact2 = generateContact(testAccount.Id);
        insert new Contact[] { testContact1, testContact2 };
        delete testContact1;

        // force initial rollup values
        forceRollup(testAccount.Id, 1);

        // undelete contact
        Test.startTest();
        undelete testContact1;
        Test.stopTest();

        // validate rollups correct
        assertRollup(testAccount.id, 2);
    }

    // validate batch works for worst case update, 200 contacts, related to 200 accounts
    // and then changed to another 200 accounts
    @isTest static void testBulkUpdate() {
        // create 400 accounts, and relate 200 contacts to first 200 accounts
        List<Account> accounts = new List<Account>();
        for(Integer i = 0; i < 400; i++) {
            accounts.add(generateAccount());
        }
        insert accounts;
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < 200; i++) {
            contacts.add(generateContact(accounts[i].Id));
        }
        insert contacts;

        // force intial rollup values
        Map<Id, Integer> initialValues = new Map<Id, Integer>();
        for(Integer i = 0; i < 400; i++) {
            initialValues.put(accounts[i].Id, (i < 200 ? 1 : 0));
        }
        forceRollup(initialValues);

        // transfer contacts to second 200 accounts
        for(Integer i = 0; i < 200; i++) {
            contacts[i].AccountId = accounts[i+200].Id;
        }
        Test.startTest();
        update contacts;
        Test.stopTest();

        // validate rollups correct
        Map<Id, Integer> expectedValues = new Map<Id, Integer>();
        for(Integer i = 0; i < 400; i++) {
            expectedValues.put(accounts[i].Id, (i < 200 ? 0 : 1));
        }
        assertRollup(expectedValues);
    }

    // Bug 1: test rollup works if parent account has null number_of_contacts
    @isTest static void testBug1NullNumberOfContacts() {
        // create account and generate related contact
        Account testAccount = createAccount();
        Contact testContact = generateContact(testAccount.Id);

        // force rollup value to null
        forceRollup(testAccount.Id, null);

        // commit contact
        Test.startTest();
        insert testContact;
        Test.stopTest();

        // validate rollups
        assertRollup(testAccount.Id, 1);
    }

    // Bug 2: no remaining contacts, can mess up aggregate patterns
    @isTest static void testBug2NoRemainingContacts() {
        // create account with related contact
        Account testAccount = createAccount();
        Contact testContact = createContact(testAccount.Id);

        // force initial rollup values
        forceRollup(testAccount.Id, 1);

        // delete last contact
        Test.startTest();
        delete testContact;
        Test.stopTest();

        // validate rollups
        assertRollup(testAccount.Id, 0);
    }

    /* Test Helpers */

    // Assertion Helpers

    private static void assertRollup(Map<Id, Integer> expectedValues) {
        List<Account> accounts = queryAccounts(expectedValues.keySet());
        for(Account account : accounts) {
            Integer expectedValue = expectedValues.get(account.Id);
            system.assertEquals(expectedValue, account.Number_Of_Contacts__c,
                    'Unexpected number of contacts');
        }
    }

    private static void assertRollup(Id accountId, Integer expected) {
        assertRollup(new Map<Id, Integer>{ accountId => expected });
    }

    // Update Helpers

    private static List<Account> forceRollup(Map<Id, Integer> rollupValues) {
        List<Account> accounts = new List<Account>();
        for(Id accountId : rollupValues.keySet()) {
            accounts.add(new Account(Id = accountId,
                    Number_Of_Contacts__c = rollupValues.get(accountId)));
        }
        update accounts;
        return accounts;
    }

    private static Account forceRollup(Id accountId, Integer rollupValue) {
        return forceRollup(new Map<Id, Integer> { accountId => rollupValue })[0];
    }

    // Query Helpers

    private static List<Account> queryAccounts(Set<Id> accountIds) {
        return [SELECT Number_Of_Contacts__c FROM Account WHERE Id in :accountIds];
    }

    private static Account queryAccount(Id accountId) {
        return queryAccounts(new Set<Id>{ accountId })[0];
    }

    // Copied from TestUtil.cls

    // Accounts
    public static Account createAccount() {
        Account account = generateAccount();
        insert account;
        return account;
    }
    public static Account generateAccount() {
        // always use long random string for name to avoid issues with de-duping packages
        return new Account(name = generateRandomString(16));
    }

    // Contacts
    public static Contact createContact(Id accountId) {
        Contact contact = generateContact(accountId);
        insert contact;
        return contact;
    }
    public static Contact generateContact(Id accountId) {
        // use long random string for last name to avoid issues with de-duping packages
        return new Contact(lastName = generateRandomString(16), accountId = accountId);
    }

    private static Set<String> priorRandoms;
    public static String generateRandomString(){return generateRandomString(null);}
    public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>();

        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return returnString;
        }
    }

}