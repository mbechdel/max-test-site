public class AddTasksExtension {

    public List<Task> tasks { get; set; }

    public AddTasksExtension(ApexPages.StandardSetController stdController) {
        tasks = new List<Task>();

        for (SObject contact : stdController.getSelected()) {
            tasks.add(new Task( WhoId = contact.Id ));
        }
    }

    public PageReference save() {
        insert tasks;
        PageReference returnPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        returnPage.setRedirect(true);
        return returnPage;
    }
}