/**
 * @author Max Bechdel <max@callaway.cloud>
 *
 * REST API skill challenge...
 *
 * Simple account REST API supporting GET/POST/PUT/DELETE.
 *     - no auth required
 *     - single records (no mass posts even though we're accepting JSON lists)
 *     - https://mprime-developer-edition.na22.force.com/services/apexrest/api/accounts/*
 *     - response codes not entirely accurate
 */
@RestResource(urlMapping='/api/accounts/*')
global with sharing class RESTAccountController {

    private static final String URL_PREFIX = 'api/accounts';
    private static RestResponse response = RestContext.response;

    /**
     * Retrieve an account.
     * @return account API object
     */
    @HttpGet
    global static AccountObject get() {
        return toAccountObject(getAccount());
    }

    /**
     * Create an account.
     * @return created API account object
     */
    @HttpPost
    global static AccountObject post() {
        AccountObject accountObject = getAccountObject();

        Account acct = new Account(
            Name = accountObject.name,
            Phone = accountObject.phone,
            AccountNumber = accountObject.accountNumber
        );
        insert acct;

        RestContext.response.statusCode = 201;
        return toAccountObject(acct);
    }

    /**
     * Update an account.
     * @return updated API account object
     */
    @HttpPut
    global static AccountObject put() {
        Account acct = getAccount();

        if (acct != null) {
            AccountObject acctObj = getAccountObject();

            acct.Name = acctObj.name;
            acct.Phone = acctObj.phone;

            update acct;

            return toAccountObject(acct);
        } else {
            return null;
        }
    }

    /**
     * Delete an account.
     */
    @HttpDelete
    global static void del() {
        Account acct = getAccount();

        if (acct != null) {
            delete acct;
            RestContext.response.statusCode = 204;
        } else {
            RestContext.response.statusCode = 404;
        }
    }

    /**
     * @return account Id from the REST request
     */
    private static Account getAccount() {
        try {
            Id accountId = RestContext.request.requestURI.substring(RestContext.request.requestURI.lastIndexOf(URL_PREFIX)+URL_PREFIX.length()+1);
            List<Account> accounts = [SELECT Id, Name, Phone, AccountNUmber FROM Account WHERE Id = :accountId];

            for (Account acct : [SELECT Id, Name, Phone, AccountNUmber FROM Account WHERE Id = :accountId]) {
                return acct;
            }
        } catch (Exception exc) {}

        return null;
    }

    /**
     * API object.
     */
    global class AccountObject {
        String id;
        String name;
        String phone;
        String accountNumber;
    }

    /**
     * Cast an account to an API account object.
     * @param  account standard object
     * @return          API object
     */
    private static AccountObject toAccountObject(Account account) {
        return (account == null ? null : ((List<AccountObject>) JSON.deserialize(JSON.serializePretty(new List<Account> { account }), List<AccountObject>.class))[0]);
    }

    /**
     * Deserializes the JSON sent over into an account API object
     * @return account API object
     */
    private static AccountObject getAccountObject() {
        List<AccountObject> acctObjs = (List<AccountObject>) JSON.deserialize(RestContext.request.requestBody.toString(), List<AccountObject>.class);
        return acctObjs.size() != 1 ? null : acctObjs[0];
    }
}