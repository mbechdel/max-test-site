public with sharing class ContactTriggerHandler {

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public ContactTriggerHandler(boolean isExecuting, integer size) {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void OnBeforeInsert(Contact[] newContacts) {
    }

    public void OnAfterInsert(Contact[] newContacts) {
        ContactTriggerHelper.incrementNumberOfContacts(newContacts);
    }

    @future public static void OnAfterInsertAsync(Set<ID> newContactIDs) {
    }

    public void OnBeforeUpdate(Contact[] oldContacts, Contact[] updatedContacts, Map<ID, Contact> ContactMap) {
    }

    public void OnAfterUpdate(Contact[] oldContacts, Contact[] updatedContacts, Map<ID, Contact> ContactMap) {
        ContactTriggerHelper.decrementNumberOfContacts(oldContacts);
        ContactTriggerHelper.incrementNumberOfContacts(updatedContacts);
    }

    @future public static void OnAfterUpdateAsync(Set<ID> updatedContactIDs) {
    }

    public void OnBeforeDelete(Contact[] ContactsToDelete, Map<ID, Contact> ContactMap) {
    }

    public void OnAfterDelete(Contact[] deletedContacts, Map<ID, Contact> ContactMap) {
        ContactTriggerHelper.decrementNumberOfContacts(deletedContacts);
    }

    @future public static void OnAfterDeleteAsync(Set<ID> deletedContactIDs) {
    }

    public void OnUndelete(Contact[] restoredContacts) {
        ContactTriggerHelper.incrementNumberOfContacts(restoredContacts);
    }

    public boolean IsTriggerContext {
        get { return m_isExecuting; }
    }

    public boolean IsVisualforcePageContext {
        get { return !IsTriggerContext; }
    }

    public boolean IsWebServiceContext {
        get { return !IsTriggerContext; }
    }

    public boolean IsExecuteAnonymousContext {
        get { return !IsTriggerContext; }
    }
}