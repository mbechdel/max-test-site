public class ContactTriggerHelper {

    /**
     * Updates an accounts Number_Of_Contacts__c field to the count of how many contacts it has under it.
     */
    public static void incrementNumberOfContacts(List<Contact> contacts) {
        Map<Id, Integer> accountsNumberOfContactsMap = getAccountNumContactsCount(contacts);
        List<Account> accountsToUpdate = queryAccounts(accountsNumberOfContactsMap.keySet());
        accountsToUpdate = setNumberOfContacts(accountsToUpdate, accountsNumberOfContactsMap, true);
        update accountsToUpdate;
    }

    /**
     * Updates an accounts Number_Of_Contacts__c field to the count of how many contacts it has under it.
     */
    public static void decrementNumberOfContacts(List<Contact> contacts) {
        Map<Id, Integer> accountsNumberOfContactsMap = getAccountNumContactsCount(contacts);
        List<Account> accountsToUpdate = queryAccounts(accountsNumberOfContactsMap.keySet());
        accountsToUpdate = setNumberOfContacts(accountsToUpdate, accountsNumberOfContactsMap, false);
        update accountsToUpdate;
    }

    private static List<Account> setNumberOfContacts(List<Account> accountsToUpdate, Map<Id, Integer> accountsNumberOfContactsMap, Boolean add) {
        for (Account account : accountsToUpdate) {
            // null check
            if (account.Number_Of_Contacts__c == null) {
                account.Number_Of_Contacts__c = 0;
            }

            if (add) {
                account.Number_Of_Contacts__c += accountsNumberOfContactsMap.get(account.Id);
            } else {
                account.Number_Of_Contacts__c -= accountsNumberOfContactsMap.get(account.Id);
            }
        }

        return accountsToUpdate;
    }

    private static List<Account> queryAccounts(Set<Id> accountIds) {
        return [SELECT Id, Number_Of_Contacts__c FROM Account WHERE Id IN :accountIds];
    }

    private static Map<Id, Integer> getAccountNumContactsCount(List<Contact> contacts) {
        Map<Id, Integer> accountsNumberOfContactsMap = new Map<Id, Integer>();

        // first get all the accounts
        for (Contact contact : contacts) {
            accountsNumberOfContactsMap.put(contact.AccountId, 0);
        }

        // now keep a counter of the number increments we need to make for that account
        for (Contact contact : contacts) {
            accountsNumberOfContactsMap.put(contact.AccountId, accountsNumberOfContactsMap.get(contact.AccountId) + 1);
        }

        return accountsNumberOfContactsMap;
    }
}