
/**
 * Trigger to create a case when a review is created that has a review
 * value below a certain threshold. Also, puts the case in a specific
 * queue.
 */
trigger CheckReviewTrigger on Book_Review__c (after insert) {
    for (Book_Review__c review : Trigger.new) {

        // check if the review is below the threshold
        if (review.Review_Value__c <= 1) {
            // create the case
            Case newCase = new Case();

            // set the book review to the case
            newCase.Book_Review__c = review.Id;

            // assign subject
            Book__c book = [select Name from Book__c where Id =: review.Book_FK__c];
            newCase.Subject = 'Low Book Review - Book: ' + book.Name;

            // assign case to the queue
            Group queue = [Select Id, Name FROM Group WHERE Type = 'Queue' and Name = 'Low Book Reviews'];
            newCase.OwnerId = queue.Id;

            // create case
            insert newCase;
        }
    }
}
