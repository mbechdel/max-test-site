
/**
 * Trigger to apply a discount whenever a new book is inserted.
 */
trigger ApplyDiscountTrigger on Book__c (before insert) {
	Book__c[] books = Trigger.new;
	HelloWorld.applyDiscount(books);
}
