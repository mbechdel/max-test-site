/**
 * HelloWorld class for learning! YAY!
 */
public with sharing class HelloWorld {
	Public String output;
	Public Map<String, String> hwMap;
	Public Static String HELLO_WORLD_GETTER = 'Hello World';
	Public Static String COMPANY_NAME_ALTERNATE = Label.company_name_alternate;

    /**
     * Constructor, sets initial values to the output and hwMap variables.
     */
	public HelloWorld() {
		output = 'Hello World';
		hwMap = new Map<String, String>{HELLO_WORLD_GETTER => ''};
	}

    /**
     * Discounts a list boos.
     *
     * @param books list of books to have discounts applied to
     */
	public static void applyDiscount(Book__c[] books) {
    	for (Book__c b :books){
        	b.Price__c *= 0.9;
     	}
    } 

    /**
     * Updates/sets a value in the hwMap map class variable.
     *
     * @param key   map index
     * @param value new value
     */
    public void setValue(String key, String value) {
    	hwMap.put(key, value);
    }

    /**
     * Returns a full list of all book names.
     *
     * @return list of all book names
     */
    public List<String> getBookNames() {
        Book__c[] dbBooks = [SELECT Name FROM Book__c];
        String[] books = new List<String>();

        for(Integer i = 0; i < dbBooks.size(); i++) {
            books.add(dbBooks[i].Name);
        }

        return books;
    }
}