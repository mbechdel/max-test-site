
@isTest
private class Test_BatchRuns {
	
	@isTest static void testBookAbbreviationBatchRuns() {
        // insert a bunch of books
        loadDefaultDataSet();

        // run the batch script to set the bookname abbreviations
        Test.startTest();
        Id batchInstanceId = Database.executeBatch(new BatchRuns());
        Test.stopTest();

        // now verify some records
        Book__c[] books = [SELECT Name, Book_Abbreviation__c FROM Book__c];

        for (Book__c book : books) {
            System.assertEquals(book.name.abbreviate(15), book.Book_Abbreviation__c, 
                'Abbreviate not correct, book name: ' + book.name + ' and abbreviation: ' + book.Book_Abbreviation__c);
        }
	}
	
    /* 
     * Loads the default data set of books.
     */
    static void loadDefaultDataSet() {
        Book__c[] books = new List<Book__c>();

        for(Integer i = 1; i <= 10; i++) {
            String bookName = generateRandomBookName();
            Book__c book = new Book__c(Name=bookName, Price__c=100);
            books.add(book);
        }

        insert books;
    }

    /*
     * Generates a random book name, 1 to 5 words, 2 to 15 characters per word.
     */
    private static String generateRandomBookName() {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        Integer numberOfWords = Math.round(Math.random() * (5 - 1)) + 1;
        String bookName = '';
        
        for(Integer i = 1; i <= numberOfWords; i++) {
            // get a random length for this word
            Integer wordLength = Math.round(Math.random() * (15 - 2)) + 2;

            for (Integer j = 0; j < wordLength; j++) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
               bookName += chars.substring(idx, idx+1);
            }

            if (i != numberOfWords) {
                bookName = bookName + ' ';
            }
        }

        return bookName;
    }
}
