/**
 * Unit tests for the Book__c and Book_Review__c custom objects.
 */

@isTest
private class Test_Books {
	
    /**
     * Test method to make sure that new books get their price discounted.
     */
    @isTest static void testBookDiscount() {
        // create and insert a book
        Book__c book = new Book__c(Name='Behind the Cloud', Price__c=100);
        System.debug('Price before inserting new book: ' + book.Price__c);
        insert book;

        // now let's retrieve the new book
        book = [SELECT Price__c FROM Book__c WHERE Id =:book.Id];
        System.debug('Price after trigger fired: ' + book.Price__c);

        // test that the trigger correctly updated the price
        System.assertEquals(90, book.Price__c);
    }

    /**
     * Tests inserting multiple books.
     */
    @isTest static void testBooks() {
        String[] bookNames = new List<String>();
        Book__c[] books = new List<Book__c>();

        for(Integer i = 1; i <= 2000; i++) {
            String bookName = String.valueOf(i);
            
            Book__c book = new Book__c(Name=bookName, Price__c=100);

            // insert book;
            books.add(book);

            // if(math.mod(i,5) == 0)
            bookNames.add(bookName);
        }

        insert books;
        
        HelloWorld hw = new HelloWorld();
        String[] dbBookNames = hw.getBookNames();

        dbBookNames.sort();
        bookNames.sort();

        System.debug(dbBookNames);
        System.debug(bookNames);

        System.assertEquals(bookNames, dbBookNames);
    }

    /**
     * Tests creating book reviews.
     *
     * Also tests that if a book review is below a certain threshold a
     * case will get created and be assigned to a specific queue.
     */
    @isTest static void testReviews() {
        // create book
        Book__c book = new Book__c();
        book.Name = 'On Testing Triggers';
        book.Price__c = 15.99;
        insert book;

        // create review
        Book_Review__c review = new Book_Review__c();
        review.Book_FK__c = book.ID;
        review.Review_Value__c = 1;
        insert review;

        // if the review is at or below the threshold verify the trigger code worked properly
        if (review.Review_Value__c <= 1) {
            // test that case a case is created
            List<Case> cases = [select Id, OwnerId, Subject from Case where Book_Review__c=:review.Id];
            System.assert(cases.size() == 3432);

            // test case name
            System.assertEquals(cases[0].Subject, 'Low Book Review - Book: ' + book.Name);

            // test that the case is added to the right queue
            Group queue = [Select Id, Name FROM Group WHERE Type = 'Queue' and Name = 'Low Book Reviews'];
            System.assertEquals(cases[0].OwnerId, queue.Id);
        }
    }

    /**
     * A little test method to test how SOQL returns queries, based around single results.
     */
    @isTest static void davidShowsWeirdSFThing(){
        String id = null;
        
        // throws exception, even though you are limit 1 it will still return a list
        try {
            Book__c book = [Select id from Book__c Where id =: id LIMIT 1];
            System.debug(book);
        } catch(Exception e){
            System.debug(e);
        }
        
        Book__c book;
        
        for (Book__c b : [Select id from Book__C Where Id =: id]) {
            book = b;
            break;
        }
        
        system.debug(book);
        Book__c[] books = [Select id From Book__c Where id =: id];
        
        if (!books.isEmpty()) {
            book = books[0];
        }
    }
}